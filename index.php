<?php
 require_once 'db_connect.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>index.php</title>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>


<input type="button" value='Добавить' onclick='addItem()' class='btn btn-info' id='add_button'>
<div id="content">
<h2>Loading...</h2>

</div>

<div id='product_dialog'>
	<form>
		<input type="hidden" name='id' />
		<div class="form-group">
			<label for="product_name">Наименование:</label>
			<input type="text" name="name" value="" class='form-control'/>
		</div>
		<div class="form-group">
			<label for="">Стоимость:</label>
			<input type="number" name="price" value="" class='form-control'/>
		</div>
		<div class="form-group">
			<label for="">Категория:</label>
			<select name="category" class='form-control'>
			<?php
				$query = "SELECT * FROM category";
				$raw = mysqli_query($mysqli, $query);
				while ($result = mysqli_fetch_assoc($raw)){
					echo "<option value='{$result['category_id']}'>".$result['category_name']."</option>";
				}
				
			?>
			</select>
		</div>
	</form>
</div>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>



</body>
</html>