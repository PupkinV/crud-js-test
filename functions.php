<?php


function fatal_error($sErrorMessage = '') {
    header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error');
    die($sErrorMessage);
}

function debug($msg){
	echo '<pre>';
	print_r($msg);
	echo '</pre>';
}