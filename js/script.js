function l(t){
	console.log(t);
}

function updateContent(){
	$.ajax({
		url : 'product.php',
		success : function(data){
			$('#content').html(data);			
		},
	});
}

function addItem(){
	$('input[name=id]').val(0);
	$('input[name=name]').val('');
	$('input[name=price]').val('');
	$('select[name=category] option').attr('selected', false);
	
	$("#product_dialog").dialog('open');
}

function editItem( id, event ){
	var cells = $(event.currentTarget)[0].cells;
	$('input[name=id]').val(cells[0].innerHTML);
	$('input[name=name]').val(cells[1].innerHTML);
	$('input[name=price]').val(cells[3].innerHTML);
	$('select[name=category] option')
		.attr('selected', false)
		.filter(function(){
				return this.text == cells[2].innerHTML;
			})
		.attr('selected', true);
	
	$("#product_dialog").dialog('open');
};

function deleteItem(item, event){
	$.ajax({
		url:'product.php?action=delete',
		data: {'id': item},
		success: function(data){
			updateContent();
		},
	});
	
	event.stopPropagation();
	return false;
};

 $(function(){//start onload
 
	updateContent();
	 $("#product_dialog").dialog({
		autoOpen:false,
		modal:true,
		create: function(){
			 $(this).css('visibility','visible');
		},
		buttons:[
			{	text:"Save",
				class: 'btn btn-success',
				click: function(){
					var action = $('input[name=id]').val()==0?"insert":"update"
					$.ajax({
						url:"product.php?action="+action,
						data: $('#product_dialog form').serialize(),
					success: function(){
						updateContent();
					}
					});
					
					$(this).dialog('close');
				}
			},
			{	text:"Close",
				class: 'btn btn-default',
				click: function(){
					$(this).dialog('close');
				},
			}
		]
	 });
	 
	
	
	
	

	
	
 })//end onload